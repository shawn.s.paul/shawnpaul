@extends('layout')
@section('content')
    <br>
    <div class="row">
        <div class="col-lg-12" style="text-align:center;">
            <h1 style="font-size:40px;">Interests</h1>
        </div>
    </div>
    <div class="row">
        @foreach ($interests as $interest)
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <div class="card text-white bg-dark mb-3">
                    <div class="card-header">
                        <h4 class="card-title">{{$interest->interest}}</h4>
                    </div>
                    <div class="card-body">
                        {{$interest->details}}
                    </div>
                    <img style="width: 100%; display: block;" src="{{$interest->image}}" alt="{{$interest->interest}}">
                </div>
            </div>
        @endforeach
    </div>
@endsection