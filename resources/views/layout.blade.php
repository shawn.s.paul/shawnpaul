<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Shawn</title>

    <!-- Fonts -->
    <link href="https://bootswatch.com/4/lux/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/css/main.css" rel="stylesheet" type="text/css">
    <script src="http://code.jquery.com/jquery-2.2.4.min.js"
            integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
            crossorigin="anonymous"></script>
    <script src="https://bootswatch.com/_vendor/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
</head>
<body class="background">
    <div class="progress">
        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
    </div>
    <div id="content" style="display:none;">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="/"><img src="/images/shawnlogo.png" style="height:25px;"/></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation" style="">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarColor03">
                <ul class="navbar-nav mr-auto">
                    @foreach ($links as $link)
                        @if ($link->link != 'interests')
                        <li class="nav-item">
                            <a class="nav-link" href="{{$link->link}}">{{$link->name}}</a>
                        </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </nav>
        <div class="container">
            @yield('content')
        </div>
    </div>
</body>
</html>
<script>
    $(document).ready(function() {
        $('.progress').slideUp(1000);
        $('#content').fadeIn(1500);
    });
</script>
