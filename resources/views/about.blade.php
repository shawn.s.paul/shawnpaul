@extends('layout')
@section('content')
    <br>
    <div class="card border-light">
        <div class="card-header">About Me</div>
        <div class="card-body">
            <p class="card-text">
                I was born and raised in Indianapolis, Indiana. My parents moved here from Pakistan in the 80s for the opportunities that my sisters and I probably couldn't have had if we were born in Pakistan.
            </p>
            <p class="card-text">
                I graduated from Pike High School on the west side of Indianapolis and went on to attend Anderson University to study Biblical Studies with the intention of becoming a youth pastor one day. During my first year of college,  I decided to enlist in the Army Indiana National Guard for the learning, experience, and educational opportunities it could provide for me. After a couple years of college, I realized I was not cut out for pastoring as a profession and decided to go back to studying computer science, which I had studied briefly in high school, though I didn't have much of an intention to end up in the computer science field.
            </p>
            <p class="card-text">
                Right after graduation, I got my first web development job at Web Connectivity. Starting out, I wasn't sure if it was the best fit, and there was a bit of a learning curve. But a few months into the job, after working very hard and learning a lot of new things, I really started to enjoy my work and developed a thirst to learn and grow even more! I was able to engineer more efficients way to accomplish the tasks that were given to me.
            </p>
            <p class="card-text">
                Since I've grown into the programming profession, I've developed a real passion for automation, making websites look sleek and modern, and making others' jobs and lives a little easier through building & improving applications.
            </p>
        </div>
    </div>
@endsection
