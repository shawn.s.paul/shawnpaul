<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    $name = "Shawn Paul";
    return view('home',compact('name','links'));
});

Route::get('/resume', function() {
    return view('resume');
});

Route::get('/about', function() {
    return view('about');
});

Route::get('/interests', function() {
    $interests = App\Interest::all();
    return view('interests',compact('interests'));
});